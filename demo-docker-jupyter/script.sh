#! /bin/bash
apt-get update;
docker run -p 8888:8888 continuumio/miniconda3 bash -c "/opt/conda/bin/conda install jupyter -y --quiet && mkdir /opt/notebooks && /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='0.0.0.0' --port=8888 --token='' --no-browser --allow-root"