# Terraform plugin for creating random ids
resource "random_id" "instance_id" {
  byte_length = 4
}
# Create VM #1
resource "google_compute_instance" "vm_instance" {
  name = "${var.app_name}-vm-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone = var.gcp_zone_1
  tags = ["ssh","http","jupyter"]



  boot_disk {
     initialize_params {
      image = var.boot_image_name
      type = "pd-standard"
    }
  }
  metadata = {
    gce-container-declaration = var.docker_declaration
  }

  labels = {
    container-vm = "cos-stable-69-10895-62-0"
  }

  metadata_startup_script = file(var.startup_script)

  network_interface {
    network = google_compute_network.vpc.name

  access_config { }
  }
}