# define the GCP authentication file
variable "gcp_auth_file" {
  type = string
  description = "GCP authentication file"
}
# define GCP project name
variable "app_project" {
  type = string
  description = "GCP project name"
}
# define application name
variable "app_name" {
  type = string
  description = "Application name"
}
# define GCP region
variable "gcp_region_1" {
  type = string
  description = "GCP region"
}
# define GCP zone
variable "gcp_zone_1" {
  type = string
  description = "GCP zone"
}

variable "docker_declaration" {
  type = string
  default = "spec:\n  containers:\n    - name: anaconda-docker\n      image: 'continuumio/miniconda3'\n      stdin: false\n      tty: false\n  restartPolicy: Always\n"
}

variable "boot_image_name" {
  type = string
  default = "projects/cos-cloud/global/images/cos-stable-69-10895-62-0"
}

variable "startup_script" {
  type = string
  default = "script.sh"
}
