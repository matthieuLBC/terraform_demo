# Terraform plugin for creating random ids
resource "random_id" "instance_id" {
  byte_length = 4
}
# Create VM #1
resource "google_compute_instance" "vm_instance" {
  name = "${var.app_name}-vm-${random_id.instance_id.hex}"
  machine_type = "f1-micro"
  zone = var.gcp_zone_1
  tags = ["ssh","http","flask"]


  boot_disk {
     initialize_params {
      image = var.boot_image_name
    }
  }

  metadata_startup_script = "sudo apt-get update; sudo apt-get install -yq build-essential python-pip rsync; pip install flask"

  network_interface {
    network = google_compute_network.vpc.name

  access_config { }
  }
}